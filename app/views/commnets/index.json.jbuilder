json.array!(@commnets) do |commnet|
  json.extract! commnet, :id, :title, :body, :post_id
  json.url commnet_url(commnet, format: :json)
end
