class CreateCommnets < ActiveRecord::Migration
  def change
    create_table :commnets do |t|
      t.string :title
      t.text :body
      t.integer :post_id

      t.timestamps
    end
    add_index :commnets, :post_id, unique: true
  end
end
